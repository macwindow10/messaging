# Messaging
This is the [stock SMS app from AOSP](https://android.googlesource.com/platform/packages/apps/Messaging/), configured as an Android Studio project. It builds with Gradle Plugin version 3.4.2 and Build Tools version 29.0 using Android Studio 3.4.2

~~This repo does not follow the upstream master branch, only certain tags.~~ This repo now follows master branch to get latest bug fixes.
